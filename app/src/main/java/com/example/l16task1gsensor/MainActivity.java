package com.example.l16task1gsensor;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {

    public static final String RED = "#e53935";
    public static final String GREEN = "#66bb6a";
    public int countOfGoodActivity = 0;
    public int countOfBadActivity = 0;
    int tmpInt;
    ImageView colorOne;
    ImageView colorTwo;
    ImageView colorThree;
    ImageView colorFour;
    ImageView colorFive;
    TextView x;
    TextView y;
    TextView z;
    TextView countOfGoodActivityView;
    TextView countOfBadActivityView;
    TextView textViewOfGoodActivity;
    TextView textViewOfBadActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        x = (TextView) findViewById(R.id.x);
        y = (TextView) findViewById(R.id.y);
        z = (TextView) findViewById(R.id.z);
        colorOne = (ImageView) findViewById(R.id.color_one);
        colorTwo = (ImageView) findViewById(R.id.color_two);
        colorThree = (ImageView) findViewById(R.id.color_three);
        colorFour = (ImageView) findViewById(R.id.color_four);
        colorFive = (ImageView) findViewById(R.id.color_five);
        textViewOfGoodActivity = (TextView) findViewById(R.id.text_view_of_good_activity);
        textViewOfBadActivity = (TextView) findViewById(R.id.text_view_of_bad_activity);
        countOfGoodActivityView = (TextView) findViewById(R.id.count_of_good_activity_value);
        countOfBadActivityView = (TextView) findViewById(R.id.count_of_bad_activity_value);

        findViewById(R.id.action_start).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, MyService.class);
                startService(intent);
            }
        });

        findViewById(R.id.action_stop).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, MyService.class);
                stopService(intent);
            }
        });

        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                new IntentFilter("custom-event-name"));
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getIntExtra("good", 0) == 1) {

                countOfGoodActivity++;
                countOfGoodActivityView.setText(countOfGoodActivity + "");
            }

            if (intent.getIntExtra("bad", 0) == 1) {

                countOfBadActivity++;
                countOfBadActivityView.setText(countOfBadActivity + "");
            }

            if((countOfGoodActivity + countOfBadActivity) == 10){

                tmpInt = Math.round(countOfGoodActivity / 2);
                setRedColors(5);
                setGreenColors(tmpInt);
                countOfGoodActivity = 0;
                countOfBadActivity = 0;
                countOfGoodActivityView.setText("0");
                countOfBadActivityView.setText("0");
            }
            x.setText(intent.getFloatExtra("x", 0) + "");
            y.setText(intent.getFloatExtra("y", 0) + "");
            z.setText(intent.getFloatExtra("z", 0) + "");
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
    }

    public void setGreenColors(int count){
        switch(count){
            case 1:
            colorOne.setBackgroundColor(Color.parseColor(GREEN));
            break;
            case 2:
            colorOne.setBackgroundColor(Color.parseColor(GREEN));
            colorTwo.setBackgroundColor(Color.parseColor(GREEN));
            break;
            case 3:
            colorOne.setBackgroundColor(Color.parseColor(GREEN));
            colorTwo.setBackgroundColor(Color.parseColor(GREEN));
            colorThree.setBackgroundColor(Color.parseColor(GREEN));
            break;
            case 4:
            colorOne.setBackgroundColor(Color.parseColor(GREEN));
            colorTwo.setBackgroundColor(Color.parseColor(GREEN));
            colorThree.setBackgroundColor(Color.parseColor(GREEN));
            colorFour.setBackgroundColor(Color.parseColor(GREEN));
            break;
            case 5:
            colorOne.setBackgroundColor(Color.parseColor(GREEN));
            colorTwo.setBackgroundColor(Color.parseColor(GREEN));
            colorThree.setBackgroundColor(Color.parseColor(GREEN));
            colorFour.setBackgroundColor(Color.parseColor(GREEN));
            colorFive.setBackgroundColor(Color.parseColor(GREEN));
            break;
        }
    }

    public void setRedColors(int count){
        switch (count) {
            case 1:
                colorFive.setBackgroundColor(Color.parseColor(RED));
                break;
            case 2:
                colorFive.setBackgroundColor(Color.parseColor(RED));
                colorFour.setBackgroundColor(Color.parseColor(RED));
                break;
            case 3:
                colorFive.setBackgroundColor(Color.parseColor(RED));
                colorFour.setBackgroundColor(Color.parseColor(RED));
                colorThree.setBackgroundColor(Color.parseColor(RED));
                break;
            case 4:
                colorFive.setBackgroundColor(Color.parseColor(RED));
                colorFour.setBackgroundColor(Color.parseColor(RED));
                colorThree.setBackgroundColor(Color.parseColor(RED));
                colorTwo.setBackgroundColor(Color.parseColor(RED));
                break;
            case 5:
                colorFive.setBackgroundColor(Color.parseColor(RED));
                colorFour.setBackgroundColor(Color.parseColor(RED));
                colorThree.setBackgroundColor(Color.parseColor(RED));
                colorTwo.setBackgroundColor(Color.parseColor(RED));
                colorOne.setBackgroundColor(Color.parseColor(RED));
                break;
        }
    }
}
