package com.example.l16task1gsensor;


import android.app.Service;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Subscription;
import rx.functions.Action1;


public class MyService extends Service implements SensorEventListener {

    public int countOfBadActivity = 0;
    public int countOfGoodActivity = 0;
    public float tmpX = 0;
    public float tmpY = 0;
    public float tmpZ = 0;
    public float [] tmpEvent = new float[3];

    SensorManager sensorManager;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Toast.makeText(this, "onCreate", Toast.LENGTH_SHORT).show();

        tmpEvent[0] = 0;
        tmpEvent[1] = 0;
        tmpEvent[2] = 0;

        Observable.interval(10, TimeUnit.MINUTES)
                .subscribe(new Action1<Long>() {
                    @Override
                    public void call(Long aLong) {
                        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
                        sensorManager.registerListener(MyService.this,
                                sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                                SensorManager.SENSOR_DELAY_NORMAL);

                    }
                });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Toast.makeText(this, "onDestroy", Toast.LENGTH_SHORT).show();
        sensorManager.unregisterListener(this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Toast.makeText(this, "onStartCommand", Toast.LENGTH_SHORT).show();

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onSensorChanged(final SensorEvent event) {
//        Log.d("Event", "x: " + event.values[0]
//                + " y: " + event.values[1]
//                + " z: " + event.values[2]);
//        double abs = Math.sqrt(event.values[0] * event.values[0]
//                + event.values[1] * event.values[1]
//                + event.values[2] * event.values[2]);
//        if (abs < 1) {
//            Log.d("Event", abs+"");
//        }

        Intent intent = new Intent("custom-event-name");
        if (((tmpX + 1 <= event.values[0]) || tmpX - 1 >= event.values[0]) ||
                (tmpY + 1 <= event.values[1] || tmpY - 1 >= event.values[1]) ||
                (tmpZ + 1 <= event.values[2] || tmpZ - 1 >= event.values[2])) {

            if (countOfGoodActivity >= 50) {
                intent.putExtra("good", 1);
                intent.putExtra("bad", 0);
                countOfGoodActivity = 0;
                sensorManager.unregisterListener(MyService.this);
            } else {
                intent.putExtra("good", 0);
                intent.putExtra("bad", 0);
            }
            countOfGoodActivity++;
        } else {
            if (countOfBadActivity >= 50) {
                intent.putExtra("bad", 1);
                intent.putExtra("good", 0);
                countOfBadActivity = 0;
                sensorManager.unregisterListener(MyService.this);
            } else {
                intent.putExtra("bad", 0);
                intent.putExtra("good", 0);
            }
            countOfBadActivity++;
        }

        // You can also include some extra data.
        intent.putExtra("x", event.values[0]);
        intent.putExtra("y", event.values[1]);
        intent.putExtra("z", event.values[2]);
        LocalBroadcastManager.getInstance(MyService.this).sendBroadcast(intent);

        tmpX = event.values[0];
        tmpY = event.values[1];
        tmpZ = event.values[2];
    }


    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}
